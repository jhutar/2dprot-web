import re


def test_home(client):
    response = client.get('/')
    assert b'2DProts' in response.data
    assert b'Database of 2D diagrams of domain secondary structures' in response.data
    assert b'Examples' in response.data
    assert b'Search 2DProts' in response.data
    assert b'ELIXIR' in response.data


def test_manual(client):
    response = client.get('/manual')
    assert b'2DProts' in response.data
    assert b'User Manual' in response.data


def test_apidoc(client):
    response = client.get('/apidoc')
    assert b'2DProts' in response.data
    assert b'API documentation' in response.data


def test_search(client):
    response = client.post('/search', data={'query': '0'})
    assert b'2DProts' in response.data
    assert b'Please provide at least 3 letters when searching' in response.data
    response = client.post('/search', data={'query': '1.40.20.10'})
    assert b'1 families found' in response.data
    assert b'0 proteins found' in response.data
    assert b'0 domains found' in response.data
    response = client.post('/search', data={'query': '2oo1'})
    assert b'0 families found' in response.data
    assert b'1 proteins found' in response.data
    assert b'0 domains found' in response.data
    response = client.post('/search', data={'query': '3e0s'})
    assert b'0 families found' in response.data
    assert b'1 proteins found' in response.data
    assert b'2 domains found' in response.data
    response = client.post('/search', data={'query': '3e0sA00'})
    assert b'0 families found' in response.data
    assert b'0 proteins found' in response.data
    assert b'1 domains found' in response.data


def test_list_family(client):
    response = client.get('/list/family')
    assert response.status_code == 200
    assert b'Listing' in response.data
    assert b'page 1' in response.data
    response = client.get('/list/family?page=1000000')
    assert response.status_code == 404


def test_list_protein(client):
    response = client.get('/list/protein')
    assert response.status_code == 200
    assert b'Listing' in response.data
    assert b'page 1' in response.data
    response = client.get('/list/protein?page=1000000')
    assert response.status_code == 404


def test_list_domain(client):
    response = client.get('/list/domain')
    assert response.status_code == 200
    assert b'Listing' in response.data
    assert b'page 1' in response.data
    response = client.get('/list/domain?page=1000000')
    assert response.status_code == 404


def test_custom_form(client):
    response = client.get('/custom')
    assert response.status_code == 200
    assert b'Create custom image' in response.data
    assert b'Submit' in response.data

    response = client.post('/custom', data={'input': ''}, follow_redirects=True)
    assert response.status_code == 200
    assert b'No domains found' in response.data

    response = client.post('/custom', data={'input': 'zzzzZ99'}, follow_redirects=True)
    assert response.status_code == 200
    assert b'Domain zzzzZ99 not found in 2DProts database' in response.data

    response = client.post('/custom', data={'input': '<script>alert()</script>'}, follow_redirects=True)
    assert response.status_code == 200
    assert b'Domain &amp;lt;script&amp;gt;alert()&amp;lt;/script&amp;gt; not found in 2DProts database' in response.data

    response = client.post('/custom', data={'input': 'zzzzZ99\n' * 31}, follow_redirects=True)
    assert response.status_code == 200
    assert b'Provided 31 rows, only 30 allowed' in response.data
