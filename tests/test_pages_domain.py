import re


def test_domain(client):
    response = client.get('/domain/this.domain.does.not.exist')
    assert response.status_code == 404
    response = client.get('/domain/3e0sA00')
    assert response.status_code == 200
    assert b'Domain 3e0sA00' in response.data
    assert re.search('Member of .*1.40.20.10', response.data.decode())
    assert b'Versions (2)' in response.data
    response = client.get('/domain/3e0sA00/latest')
    assert response.status_code == 308
    response = client.get('/domain/3e0sA00/this-version-does-not-exist')
    assert response.status_code == 404
    response = client.get('/domain/3e0sA00/2021-10-05T12_17_39_510846128_02_00')
    assert response.status_code == 200
    assert b'Domain 3e0sA00' in response.data
    assert re.search('Member of .*1.40.20.10', response.data.decode())
    assert b'Versions (2)' in response.data
    assert '« no prev.'.encode() in response.data
    assert '3e0sB00 »'.encode() in response.data

def test_domain_with_without_protein(client):
    response = client.get('/domain/3e0sA00')   # protein 3e0s exists in the DB
    assert response.status_code == 200
    assert b'Part of protein: <a href="/protein/3e0s">3e0s</a>' in response.data
    response = client.get('/domain/6qv7A00')   # protein 6qv7 missing in the DB
    assert response.status_code == 200
    assert b'No protein info' in response.data

def test_domain_files(client):
    response = client.get('/files/domain/this.domain.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/domain/3e0sA00/this.version.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/domain/3e0sA00/2021-10-05T12_17_39_510846128_02_00/this-svg-does-not-exist.svg')
    assert response.status_code == 404
    response = client.get('/files/domain/3e0sA00/2021-10-05T12_17_39_510846128_02_00/svg', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/domain/3e0sA00/2021-10-05T12_17_39_510846128_02_00/json', follow_redirects=True)
    assert response.status_code == 200
    assert response.is_json
    assert 'metadata' in response.get_json()
    assert 'sses' in response.get_json()

def test_domain_lowercase(client):
    response = client.get('/domain/3fnda02')
    assert response.status_code == 200
    assert b'Domain 3fnda02' in response.data
    assert re.search('Member of .*3.40.5.30', response.data.decode())
    assert b'Versions (2)' in response.data
    response = client.get('/domain/3fnda02/latest')
    assert response.status_code == 308
    response = client.get('/domain/3fnda02/this-version-does-not-exist')
    assert response.status_code == 404
    response = client.get('/domain/3fnda02/2021-10-05T15_38_54_451334428_02_00')
    assert response.status_code == 200
