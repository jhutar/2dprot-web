import re


def test_family(client):
    response = client.get('/family/this.family.does.not.exist')
    assert response.status_code == 404
    response = client.get('/family/1.40.20.10')
    assert response.status_code == 200
    assert b'Family 1.40.20.10' in response.data
    assert b'Domains (4)' in response.data
    assert b'Clusters (1)' in response.data
    assert b'Versions (2)' in response.data
    response = client.get('/family/1.40.20.10/latest')
    assert response.status_code == 308
    response = client.get('/family/1.40.20.10/this-version-does-not-exist')
    assert response.status_code == 404
    response = client.get('/family/1.40.20.10/2020-10-01T20_47_11_553460577_02_00')
    assert response.status_code == 200
    assert b'Family 1.40.20.10' in response.data
    assert b'Domains (4)' in response.data
    assert b'Clusters (0)' in response.data
    assert b'Versions (2)' in response.data
    response = client.get('/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00')
    assert response.status_code == 200
    assert b'Family 1.40.20.10' in response.data
    assert b'Domains (4)' in response.data
    assert b'Clusters (1)' in response.data
    assert b'Versions (2)' in response.data


def test_family_version(client):
    response = client.get('/family/1.40.20.10/latest', follow_redirects=True)
    pattern = re.compile('\<strong\>\<a href="[^"]+"\>2021-10-05T12_17_39_510846128_02_00\</a\>\</strong\>', re.MULTILINE)
    assert response.status_code == 200
    assert pattern.search(response.data.decode()) is not None
    response = client.get('/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00')
    assert response.status_code == 200
    assert pattern.search(response.data.decode()) is not None
    response = client.get('/family/1.40.20.10/2020-10-01T20_47_11_553460577_02_00')
    pattern = re.compile('\<strong\>\<a href="[^"]+"\>2020-10-01T20_47_11_553460577_02_00\</a\>\</strong\>', re.MULTILINE)
    assert response.status_code == 200
    assert pattern.search(response.data.decode()) is not None


def test_family_files(client):
    response = client.get('/files/family/this.family.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/family/1.40.20.10/this.version.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00/this-svg-does-not-exist.svg')
    assert response.status_code == 404
    response = client.get('/files/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00/multiple.svg', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00/multiple2.svg', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00/multiple3.svg', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00/domain_list.txt', follow_redirects=True)
    assert response.status_code == 404   # FIXME
    response = client.get('/files/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00/ligands.json', follow_redirects=True)
    assert response.status_code == 200
    assert b'{"name":"' in response.data


def test_family_domains_csv(client):
    response = client.get('/family/1.40.20.10/this.version.does.not.exist')
    assert response.status_code == 404
    response = client.get('/family/1.40.20.10/2021-10-05T12_17_39_510846128_02_00.csv')
    assert response.status_code == 200
    assert b'3e0sA00' in response.data
    assert b'6qvaA00' in response.data
    assert 'text/csv' in response.content_type


def test_family_with_lowercased_domain(client):
    response = client.get('/family/3.40.5.30')
    assert response.status_code == 200
    assert b'/domain/3fnda02' in response.data
