import re


def test_protein(client):
    response = client.get('/protein/this.protein.does.not.exist')
    assert response.status_code == 404
    response = client.get('/protein/2oo0')
    assert response.status_code == 200
    assert b'Protein 2oo0' in response.data
    assert b'Versions (1)' in response.data
    response = client.get('/protein/2oo0/latest')
    assert response.status_code == 308
    response = client.get('/protein/2oo0/this-version-does-not-exist')
    assert response.status_code == 404
    response = client.get('/protein/2oo0/2021-10-07T11_59_12')
    assert response.status_code == 200
    assert b'3D model (<a href="https://www.ebi.ac.uk/pdbe/entry/pdb/2oo0">PDBe</a>)' in response.data
    assert b'Protein 2oo0' in response.data
    assert b'<a href="http://cathdb.info/pdb/2oo0">CATH</a> domains (0)' in response.data
    assert b'Versions (1)' in response.data


def test_protein_with_domains(client):
    response = client.get('/protein/3e0s/2022-09-18T18_36_00')
    assert response.status_code == 200
    assert b'Protein 3e0s' in response.data
    assert b'<a href="http://cathdb.info/pdb/3e0s">CATH</a> domains (2)' in response.data


def test_protein_files(client):
    response = client.get('/files/protein/this.protein.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/protein/2oo0/this.version.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/protein/2oo1/2021-10-07T11_59_31/this-svg-does-not-exist.svg')
    assert response.status_code == 404
    response = client.get('/files/protein/2oo1/latest/svg', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/protein/2oo1/2021-10-07T11_59_31/svg', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/protein/3e0s/svg', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/protein/3e0s/svg_l', follow_redirects=True)
    assert response.status_code == 200
    assert b'<svg ' in response.data
    response = client.get('/files/protein/2oo1/2021-10-07T11_59_31/chains.json', follow_redirects=True)
    assert response.status_code == 200
    assert 'application/json' in response.content_type
    assert response.is_json
    assert response.get_json()['protein'] == '2oo1'
    assert response.get_json()['version'] == '2021-10-07T11_59_31'
    assert len(response.get_json()['chains']) == 4
    assert {'chain': 'D'} in response.get_json()['chains']


def test_proteins_with_same_version(client):
    response = client.get('/protein/4azy')
    assert response.status_code == 200
    response = client.get('/protein/4azz')
    assert response.status_code == 200
