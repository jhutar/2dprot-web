import re


def test_cluster(client):
    response = client.get('/cluster/this.cluster.does.not.exist')
    assert response.status_code == 404
    response = client.get('/cluster/1.40.20.10.1')
    assert response.status_code == 200
    assert b'Cluster 1.40.20.10.1' in response.data
    assert re.search('Member of .*1.40.20.10', response.data.decode())
    assert b'Domains not tracked' in response.data
    response = client.get('/cluster/1.40.20.10.1/latest')
    assert response.status_code == 308
    response = client.get('/cluster/1.40.20.10.1/this-version-does-not-exist')
    assert response.status_code == 404
    response = client.get('/cluster/1.40.20.10.1/2021-10-05T12_17_39_510846128_02_00')
    assert response.status_code == 200
    assert b'Cluster 1.40.20.10.1' in response.data
    assert re.search('Member of .*1.40.20.10', response.data.decode())
    assert b'Domains not tracked' in response.data

def test_cluster_files(client):
    response = client.get('/files/cluster/this.cluster.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/cluster/1.40.20.10.1/this.file.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/cluster/1.40.20.10.1/this.version.does.not.exist/ligands.json')
    assert response.status_code == 404
    response = client.get('/files/cluster/1.40.20.10.1/2021-10-05T12_17_39_510846128_02_00/this.file.does.not.exist')
    assert response.status_code == 404
    response = client.get('/files/cluster/1.40.20.10.1/ligands.json', follow_redirects=True)
    assert response.status_code == 200
    assert b'{"name":"' in response.data
    response = client.get('/files/cluster/1.40.20.10.1/2021-10-05T12_17_39_510846128_02_00/ligands.json', follow_redirects=True)
    assert response.status_code == 200
    assert b'{"name":"' in response.data
    response = client.get('/files/cluster/2.40.50.1000.1/2021-05-02T07_57_58_655839205_02_00/ligands.json', follow_redirects=True)
    assert response.status_code == 404
