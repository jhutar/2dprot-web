import os

import pytest

import two_d_prot_ui

@pytest.fixture
def app():
    os.environ['FLASK_ENV'] = 'testing'
    app = two_d_prot_ui.create_app()
    yield app

@pytest.fixture
def client(app):
    two_d_prot_ui.models.db.app_db.init_app(app)
    return app.test_client()
