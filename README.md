2DProt Web
==========

For 2DProts web database see: https://2dprots.ncbr.muni.cz/

For git repository with the code to generate actual 2D diagrams see: https://gitlab.com/jhutar/2dprot

Local development
-----------------

First start Redis (for Celery) and PostgreSQL (for both this app and Celery):

    $ sudo podman run --rm -ti --name redis_database -p 6379:6379 quay.io/centos7/redis-5-centos7
    $ sudo podman run --rm -ti --name postgresql_database -e POSTGRESQL_USER=user -e POSTGRESQL_PASSWORD=pass -e POSTGRESQL_DATABASE=sqlalchemy -p 5432:5432 quay.io/centos7/postgresql-13-centos7
    $ sudo podman exec -ti postgresql_database bash -c 'createdb --owner=user celery'

Clone 2dprot directory:

    cd two_d_prot_ui/
    git clone https://gitlab.com/jhutar/2dprot.git
    cd ..

Install venv:

    python -m venv venv --system-site-packages
    source venv/bin/activate
    pip install -r requirements.txt
    pip install -r two_d_prot_ui/2dprot/requirements.txt

If venv already created, just activate it:

    source venv/bin/activate

Link development version of the config to current directoy:

    ln -s devops/configs/two_d_prot.devel/config.json config.json

Create basic directories (optionally add some content into it):

    mkdir -p instance/2DProt/families
    mkdir -p instance/2DProt/proteins

Initiate DB and import families and proteins if added earlier (that
optional step):

    FLASK_APP=two_d_prot_ui python3 -m flask init-db
    FLASK_APP=two_d_prot_ui python3 -m flask import-families
    FLASK_APP=two_d_prot_ui python3 -m flask import-proteins

Make sure development version of Flask is able to serve static files
(normally handled by Nginx):

    cd two_d_prot_ui/static/
    ln -s ../../instance/2DProt 2DProt
    cd ../../

Finally, run the web application:

    FLASK_APP=two_d_prot_ui python3 -m flask run

Handy command to connect to the DB:

    $ PGPASSWORD=pass psql --host localhost --port 5432 --user user sqlalchemy
    $ pg_dump --host localhost --port 5432 --user user --dbname sqlalchemy >backup-before.sq

Or if using SQLite backend:

    $ sqlite3 /tmp/test.db
    sqlite> .output /tmp/backup.sql
    sqlite> .dump
    sqlite> .quit

Data structure
--------------

Data in the `web/` direcotry should be present in this structure:

    web/
      +-- generated-1.1.1.1
      |     +-- <version_1>
      |     |     +-- images and such
      |     +-- <version_2>
      +-- generated-1.1.1.2
      [...]

so once you put new family versions to the data directory, run
`scripts/reorder-web.sh` to reorder direcotries like:

    web/
      +-- generated-2.20.220.10-2020-12-23T18_48_36_986881802_01_00
      +-- generated-2.20.220.10-2020-12-27T21_02_38_185756848_01_00

to propper directory structure:

    web/
      +-- generated-2.20.220.10/
            +-- 2020-12-23T18_48_36_986881802_01_00
            +-- 2020-12-27T21_02_38_185756848_01_00

Deployment
----------

Deploy using Ansible:

    $ ansible-playbook \
        -i devops/inventory.ini \
        -e FLASK_PROMETHEUS_METRICS_PASSWORD=reset \
        -e NGINX_PROMETHEUS_BASIC_AUTH_PASSWORD=reset \
        -e containers_pull=true \
        -e letsencrypt_update=true \
        devops/playbooks/containers-upgrade.yaml

DB migrations
-------------

Initial migration:

    FLASK_APP=two_d_prot_ui python3 -m flask db init
    FLASK_APP=two_d_prot_ui python3 -m flask db migrate -m "Initial migration."
    FLASK_APP=two_d_prot_ui python3 -m flask db upgrade
    git add migrations/

When you do a DB schema change:

    FLASK_APP=two_d_prot_ui python3 -m flask db migrate -m "feat(db): Adding cluster to domains mapping"
    FLASK_APP=two_d_prot_ui python3 -m flask db upgrade
    git add migrations/versions/6b84daf05f5c_feat_db_adding_cluster_to_domains_.py

Running unit tests
------------------

Given PostgreSQL and Redis is running, cleanup the environment:

    FLASK_APP=two_d_prot_ui python3 -m flask init-db
    rm -rf instance/2DProt/*
    cp -r tests/test_data1/* instance/2DProt/
    FLASK_APP=two_d_prot_ui python3 -m flask import-families
    FLASK_APP=two_d_prot_ui python3 -m flask import-proteins

Finally, run the tests:

    python -m pytest tests/

To run the tests with output not hidden:

    python -m pytest --capture=no -o log_cli=True tests/

Troubleshooting
---------------

Remove `FamilyVersion`s with no domains:

    $ flask shell
    from two_d_prot_ui import db
    from two_d_prot_ui import models
    for i in models.FamilyVersion.query.all():
      if len(i.domains) == 0:
        print(">>> >>> >>>", i.family, i.version)
        for ii in i.clusters:
            db.app_db.session.delete(ii)
        db.app_db.session.delete(i.version)
        db.app_db.session.delete(i)
        db.app_db.session.commit()


Some Celery related knowleadge
------------------------------

Start Celery worker:



Run Celery task manually:

    sudo podman exec -ti celery-worker-2dprot sh
    $ celery shell
    >>> import two_d_prot_ui.tasks
    >>> two_d_prot_ui.tasks.pdbe_api_snapshot_all_families.apply()
