import uuid

from flask import current_app

from sqlalchemy.sql.expression import func

from sqlalchemy_utils import UUIDType

from . import db


class Version(db.app_db.Model):
    id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    name = db.app_db.Column(db.app_db.String(100), unique=True, nullable=False)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    families = db.app_db.relationship('FamilyVersion', back_populates='version')
    proteins = db.app_db.relationship('ProteinVersion', back_populates='version')
    clusters = db.app_db.relationship('ClusterVersion', back_populates='version')

    def __repr__(self):
        return '<Version %r@%r>' % (self.name, self.id)


class FamilyVersion(db.app_db.Model):
    family_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('family.id'), primary_key=True)
    version_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('version.id'), primary_key=True)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    family = db.app_db.relationship('Family', back_populates='versions')
    version = db.app_db.relationship('Version', back_populates='families')
    domains = db.app_db.relationship('DomainFamilyVersion', back_populates='familyversion')
    clusters = db.app_db.relationship('ClusterFamilyVersion', back_populates='familyversion')

    def __repr__(self):
        return '<FamilyVersion %r, %r>' % (self.family_id, self.version_id)


class Family(db.app_db.Model):
    id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    name = db.app_db.Column(db.app_db.String(100), unique=True, nullable=False)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    deleted = db.app_db.Column(db.app_db.Boolean(), nullable=False, default=False)
    versions = db.app_db.relationship('FamilyVersion', back_populates='family')

    def __repr__(self):
        return '<Family %r@%r>' % (self.name, self.id)


class DomainFamilyVersion(db.app_db.Model):
    domain_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('domain.id'), primary_key=True)
    family_id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    version_id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    familyversion = db.app_db.relationship('FamilyVersion', back_populates='domains')
    domain = db.app_db.relationship('Domain', back_populates='familyversions')
    __table_args__ = (
        db.app_db.ForeignKeyConstraint(
            ['version_id', 'family_id'],
            ['family_version.version_id', 'family_version.family_id'],
        ),
    )

    def __repr__(self):
        return '<DomainFamilyVersion %r, %r, %r>' % (self.domain_id, self.family_id, self.version_id)


class Domain(db.app_db.Model):
    id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    name = db.app_db.Column(db.app_db.String(100), unique=True, nullable=False)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    deleted = db.app_db.Column(db.app_db.Boolean(), nullable=False, default=False)
    familyversions = db.app_db.relationship('DomainFamilyVersion', back_populates='domain')
    clusterdomains = db.app_db.relationship('ClusterDomainVersion', back_populates='domain')
    def __repr__(self):
        return '<Domain %r@%r>' % (self.name, self.id)

    def get_protein_name(self):
        """Guess protein name for this domain"""
        return self.name[:4]


class ClusterFamilyVersion(db.app_db.Model):
    cluster_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('cluster.id'), primary_key=True)
    family_id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    version_id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    familyversion = db.app_db.relationship('FamilyVersion', back_populates='clusters')
    cluster = db.app_db.relationship('Cluster', back_populates='familyversions')
    __table_args__ = (
        db.app_db.ForeignKeyConstraint(
            ['version_id', 'family_id'],
            ['family_version.version_id', 'family_version.family_id'],
        ),
    )

    def __repr__(self):
        return '<ClusterFamilyVersion %r, %r, %r>' % (self.cluster_id, self.family_id, self.version_id)


class Cluster(db.app_db.Model):
    id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    name = db.app_db.Column(db.app_db.String(100), unique=True, nullable=False)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    deleted = db.app_db.Column(db.app_db.Boolean(), nullable=False, default=False)
    familyversions = db.app_db.relationship('ClusterFamilyVersion', back_populates='cluster')
    version = db.app_db.relationship('ClusterVersion', back_populates='cluster')

    def __repr__(self):
        return '<Cluster %r@%r>' % (self.name, self.id)

class ClusterVersion(db.app_db.Model):
    cluster_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('cluster.id'), primary_key=True)
    version_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('version.id'), primary_key=True)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    cluster = db.app_db.relationship('Cluster', back_populates='version')
    version = db.app_db.relationship('Version', back_populates='clusters')
    clusterdomains = db.app_db.relationship('ClusterDomainVersion', back_populates='clusterversion')

    def __repr__(self):
        return '<ClusterVersion %r, %r>' % (self.cluster_id, self.version_id)


class ClusterDomainVersion(db.app_db.Model):
    domain_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('domain.id'),primary_key=True)
    cluster_id = db.app_db.Column(db.app_db.Integer,  primary_key=True)
    version_id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    clusterversion = db.app_db.relationship('ClusterVersion', back_populates='clusterdomains')
    domain = db.app_db.relationship('Domain', back_populates='clusterdomains')
    __table_args__ = (
        db.app_db.ForeignKeyConstraint(
            ['version_id', 'cluster_id'],
            ['cluster_version.version_id', 'cluster_version.cluster_id'],
        ),
    )

    def __repr__(self):
        return '<ClusterDomainVersion %r, %r, %r>' % (self.cluster_id, self.domain_id, self.version_id)


class ProteinVersion(db.app_db.Model):
    protein_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('protein.id'), primary_key=True)
    version_id = db.app_db.Column(db.app_db.Integer, db.app_db.ForeignKey('version.id'), primary_key=True)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    protein = db.app_db.relationship('Protein', back_populates='versions')
    version = db.app_db.relationship('Version', back_populates='proteins')

    def __repr__(self):
        return '<ProteinVersion %r, %r>' % (self.protein_id, self.version_id)


class Protein(db.app_db.Model):
    id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    name = db.app_db.Column(db.app_db.String(100), unique=True, nullable=False)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    deleted = db.app_db.Column(db.app_db.Boolean(), nullable=False, default=False)
    versions = db.app_db.relationship('ProteinVersion', back_populates='protein')

    def __repr__(self):
        return '<Protein %r@%r>' % (self.name, self.id)


class CustomJob(db.app_db.Model):
    id = db.app_db.Column(UUIDType(binary=False), primary_key=True, default=uuid.uuid4)
    status = db.app_db.Column(db.app_db.String(100), nullable=False)
    imported_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    family_str = db.app_db.Column(db.app_db.String(100), nullable=False)
    domains_str = db.app_db.Column(db.app_db.String(10000), nullable=False)
    deleted = db.app_db.Column(db.app_db.Boolean(), nullable=False, default=False)

    def __repr__(self):
        return '<CustomJob %r>' % (self.id,)

    @property
    def name(self):
        return str(self.id)

class PdbeApiSnapshotAllFamilies(db.app_db.Model):
    id = db.app_db.Column(db.app_db.Integer, primary_key=True)
    snapshot = db.app_db.Column(db.app_db.String(100), unique=True, nullable=False)
    status = db.app_db.Column(db.app_db.String(100), nullable=False)
    started_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=False, server_default=func.now())
    finished_at = db.app_db.Column(db.app_db.DateTime(timezone=True), nullable=True)
    families_count = db.app_db.Column(db.app_db.Integer, nullable=False)
    families_successful = db.app_db.Column(db.app_db.Integer, nullable=False)
    families_failed = db.app_db.Column(db.app_db.Integer, nullable=False)
    celery_id = db.app_db.Column(UUIDType(binary=False), nullable=False)

    def __repr__(self):
        return '<PdbeApiSnapshotAllFamilies %r, %r>' % (self.id, self.snapshot)

    def as_dict(self):
        return {
            'snapshot': self.snapshot,
            'status': self.status,
            'started_at': self.started_at,
            'finished_at': self.finished_at,
            'families_count': self.families_count,
            'families_successful': self.families_successful,
            'families_failed': self.families_failed,
            'celery_id': self.celery_id,
        }
