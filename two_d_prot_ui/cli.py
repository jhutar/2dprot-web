import os
import re

import click

from flask import current_app
from flask.cli import with_appcontext

from sqlalchemy.sql.expression import (
    and_,
)

from . import db
from . import models
from . import utils


def create_schema():
    current_app.logger.info(f"Creating tables {','.join([i.name for i in db.app_db.metadata.sorted_tables])}")
    db.app_db.create_all()


def drop_schema():
    current_app.logger.info(f"Dropping tables {','.join([i.name for i in db.app_db.metadata.sorted_tables])}")
    db.app_db.drop_all()


def import_families():
    imported_families = 0
    imported_family_versions = 0
    imported_domains = 0
    imported_domain_versions = 0
    imported_clusters = 0
    imported_cluster_versions = 0

    print("fam: na zacatku         ")
    if not os.path.exists(current_app.config['SOURCE_FAMILIES']):
        os.makedirs(current_app.config['SOURCE_FAMILIES'])

    db.app_db.session.autoflush = False

    for gendir in os.listdir(current_app.config['SOURCE_FAMILIES']):
        print("fam gendir", gendir)
        if not re.match(current_app.config['SOURCE_FAMILIES_GENDIR_REGEXP'], gendir):
            current_app.logger.warning(f"Wait, {gendir} does not match expected pattern, maybe it is some empty dir to be removed?")
            continue

        family = gendir.split('-')[1]
        db_family = models.Family.query.filter_by(name=family).first()
        if db_family is None:
            db_family = models.Family(name=family)
            imported_families += 1

            current_app.logger.debug(f"Inserting {db_family}")
            db.app_db.session.add(db_family)

        print("fam: ", os.path.join(current_app.config['SOURCE_FAMILIES'], gendir))
        for version in os.listdir(os.path.join(current_app.config['SOURCE_FAMILIES'], gendir)):
            if version.startswith(f'{family}.svg'):
                continue

            generated = os.path.join(current_app.config['SOURCE_FAMILIES'], gendir, version)
            if not os.path.isdir(generated):
                current_app.logger.warning(f"Wait, {generated} is not a directory, maybe it is some mess to remove?")
                continue
            print("fam: genberated", generated)

            generated_listdir = os.listdir(generated)
            is_there = False
            for generated_listdir_file in generated_listdir:
#                print("fam: generated_listdir_file",generated_listdir_file , re.match(current_app.config['SOURCE_FAMILIES_VERSION_PROOVE_REGEXP'], generated_listdir_file), )
                if re.match(current_app.config['SOURCE_FAMILIES_VERSION_PROOVE_REGEXP'], generated_listdir_file):
                    is_there = True
                
            print("fam: is_there 1", is_there, family)
            if is_there == False:
                current_app.logger.warning(f"Wait, {generated} does not contain '{current_app.config['SOURCE_FAMILIES_VERSION_PROOVE_REGEXP']}', maybe it is some empty dir to be removed?")
                continue
            print("fam: is_there", family)

            if db_family.id is None:
                db_versions = []
            else:
                db_versions = models.Version.query \
                    .join(models.FamilyVersion) \
                    .filter(and_(models.FamilyVersion.family==db_family,
                                 models.Version.name==version)) \
                    .order_by(models.Version.name.desc()) \
                    .all()
            if len(db_versions) > 1:
                current_app.logger.warning(f"Wait, there are more then one of versions {version} for family {db_family}, skipping: {db_versions}")
                continue
            if len(db_versions) == 1:
                if current_app.config['IMPORT_FAMILIES_THOROUGHLY']:
                    current_app.logger.debug(f"{db_family}/{db_versions[0]} already exists in the DB, using that")
                else:
                    current_app.logger.debug(f"{db_family}/{db_versions[0]} already exists in the DB, skipping")
                    continue

            db_version = models.Version.query.filter_by(name=version).first()
            if db_version is None:
                db_version = models.Version(name=version)

            db_family_version = utils.query_no_value_safe(
                models.FamilyVersion.query \
                    .filter(and_(models.FamilyVersion.family==db_family,
                                 models.FamilyVersion.version==db_version)) \
                    .first
            )
            if db_family_version is None:
                current_app.logger.debug(f"Inserting {db_version} into {db_family}")
                db_family_version = models.FamilyVersion()
                imported_family_versions += 1
                db_family_version.family = db_family
                db_version.families.append(db_family_version)
                db.app_db.session.add(db_version)
                db.app_db.session.add(db_family_version)

            db.app_db.session.flush()

            for f in generated_listdir:

                print("dom file", f)
                # Process all the families
                domain_match = re.match(current_app.config['SOURCE_FAMILIES_DOMAIN_SVG_REGEXP'], f)
                print("dom file", f, domain_match)
                if domain_match:
                    prot = domain_match.group(1)
                    chain = domain_match.group(2)
                    dom = domain_match.group(3)
                    domain = prot + chain + dom

                    db_domain = models.Domain.query.filter_by(name=domain).first()
                    if db_domain is None:
                        db_domain = models.Domain(name=domain)
                        imported_domains += 1

                    db_domain_family_version = utils.query_no_value_safe(
                        models.DomainFamilyVersion.query \
                            .filter(and_(models.DomainFamilyVersion.familyversion==db_family_version,
                                         models.DomainFamilyVersion.domain==db_domain)) \
                            .first
                        )
                    if db_domain_family_version is None:
                        current_app.logger.debug(f"Inserting {db_domain} into {db_family}/{db_version}")
                        db_domain_family_version = models.DomainFamilyVersion()
                        db_domain_family_version.domain = db_domain
                        db_family_version.domains.append(db_domain_family_version)
                        db.app_db.session.add(db_domain_family_version)
                        imported_domain_versions += 1

                    if imported_domain_versions % 100 == 0:
                        db.app_db.session.flush()

                    continue

                # Process all family clusters
                cluster_match = re.match(current_app.config['SOURCE_FAMILIES_CLUSTER_DIR_REGEXP'], f)
                if cluster_match:
                    cluster = cluster_match.group(1)

                    ff = os.path.join(generated, cluster, 'imageCircle-multiple.png')
                    if not os.path.exists(ff):
                        current_app.logger.warning(f"{db_family}/{db_version} contains cluster {cluster}, but it does not contain image")

                    db_cluster = models.Cluster.query.filter_by(name=cluster).first()
                    if db_cluster is None:
                        db_cluster = models.Cluster(name=cluster)
                        imported_clusters += 1

                    db_cluster_family_version = utils.query_no_value_safe(
                        models.ClusterFamilyVersion.query \
                            .filter(and_(models.ClusterFamilyVersion.familyversion==db_family_version,
                                         models.ClusterFamilyVersion.cluster==db_cluster)) \
                            .first
                    )
                    if db_cluster_family_version is None:
                        current_app.logger.debug(f"Inserting {db_cluster} into {db_family}/{db_version}")
                        db_cluster_family_version = models.ClusterFamilyVersion()
                        db_cluster_family_version.cluster = db_cluster
                        db_family_version.clusters.append(db_cluster_family_version)
                        db.app_db.session.add(db_cluster_family_version)
                        imported_cluster_versions += 1

                    db_cluster_version = utils.query_no_value_safe(
                        models.ClusterVersion.query \
                            .filter(and_(models.ClusterVersion.version==db_version,
                                         models.ClusterVersion.cluster==db_cluster)) \
                            .first
                    )
                    if db_cluster_version is None:
                        db_cluster_version = models.ClusterVersion()
                        db_cluster_version.cluster = db_cluster
                        db_cluster_version.version = db_version
                        db_version.clusters.append(db_cluster_version)

                    db.app_db.session.flush()

                    # read all clusters domain from domain_list.txt
                    ff = os.path.join(generated, cluster, 'domain_list.txt')
                    if not os.path.exists(ff):
                        current_app.logger.warning(f"{db_family}/{db_version} contains cluster {cluster}, but it does not contain domain_list.txt")
                    else:
                       # read domain_list.txt and for each pcd add a domain to this cluster
                        with open(ff, "r") as fd:
                            line = fd.readline()
                            while line:
                                domain_split = line.split(",")
                                domain = domain_split[0][2:-1]+domain_split[1][2:-1]+domain_split[2][2:-3]

                                db_domain = models.Domain.query.filter_by(name=domain).first()
                                if db_domain is None:
                                    db_domain = models.Domain(name=domain)

                                db_cluster_domain_version = utils.query_no_value_safe(
                                    models.ClusterDomainVersion.query \
                                        .filter(and_(models.ClusterDomainVersion.domain==db_domain,
                                                     models.ClusterDomainVersion.clusterversion==db_cluster_version)) \
                                        .first
                                )
                                if db_cluster_version is None:
                                    current_app.logger.debug(f"Inserting {db_domain} into {db_cluster}/{db_version}")
                                    db_cluster_domain_version = models.ClusterDomainVersion()
                                    db_cluster_domain_version.domain = db_domain
                                    db_cluster_version.clusterdomains.append(db_cluster_domain_version)
                                    db.app_db.session.flush()
                                line = fd.readline()
                    continue

        db.app_db.session.commit()

    current_app.logger.info(f"Imported {imported_families} new families and {imported_family_versions} new family versions, {imported_domains} new domains and {imported_domain_versions} new domain versions, {imported_clusters} new clusters and {imported_cluster_versions} new cluster versions")
    return imported_families + imported_family_versions + imported_domains + imported_domain_versions + imported_clusters + imported_cluster_versions


def import_proteins():
    imported_proteins = 0
    imported_protein_versions = 0

    if not os.path.exists(current_app.config['SOURCE_PROTEINS']):
        os.makedirs(current_app.config['SOURCE_PROTEINS'])

    db.app_db.session.autoflush = False

    for gendir in os.listdir(current_app.config['SOURCE_PROTEINS']):
        if re.match(current_app.config['SOURCE_PROTEINS_GENDIR_REGEXP'], gendir):
            protein = gendir.split('-')[1]

            db_protein = models.Protein.query.filter_by(name=protein).first()
            if db_protein is None:
                db_protein = models.Protein(name=protein)
                imported_proteins += 1

            for version in os.listdir(os.path.join(current_app.config['SOURCE_PROTEINS'], gendir)):
                generated = os.path.join(current_app.config['SOURCE_PROTEINS'], gendir, version)
                if not os.path.isdir(generated):
                    current_app.logger.warning(f"Wait, {generated} is not a directory, maybe it is some mess to remove?")
                    continue

                if db_protein.id is None:
                    db_versions = []
                else:
                    db_versions = models.Version.query \
                        .join(models.ProteinVersion) \
                        .filter(and_(models.ProteinVersion.protein==db_protein,
                                     models.Version.name==version)) \
                        .order_by(models.Version.name.desc()) \
                        .all()
                if len(db_versions) > 1:
                    current_app.logger.warning(f"Wait, there are more then one of versions {version} for protein {db_protein}, skipping: {db_versions}")
                    continue
                if len(db_versions) == 1:
                    current_app.logger.debug(f"{db_protein}/{db_versions[0]} already exists in the DB, skipping")
                    continue

                db_version = models.Version.query.filter_by(name=version).first()
                if db_version is None:
                    db_version = models.Version(name=version)
                imported_protein_versions += 1

                current_app.logger.debug(f"Inserting {db_version} into {db_protein}")
                db_protein_version = models.ProteinVersion()
                db_protein_version.protein = db_protein
                db_version.proteins.append(db_protein_version)
                db.app_db.session.add(db_protein)
                db.app_db.session.add(db_protein_version)

                db.app_db.session.commit()

    current_app.logger.info(f"Imported {imported_proteins} new proteins and {imported_protein_versions} new protein versions")
    return imported_proteins + imported_protein_versions


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    drop_schema()
    create_schema()
    click.echo('Initialized the database.')


@click.command('import-families')
@with_appcontext
def import_families_command():
    """Import new families and family versions to the DB."""
    count = import_families()
    click.echo(f'Imported families ({count} objects).')


@click.command('import-proteins')
@with_appcontext
def import_proteins_command():
    """Import new proteins and protein versions to the DB."""
    count = import_proteins()
    click.echo(f'Imported proteins ({count} objects).')


def init_app():
    current_app.cli.add_command(init_db_command)
    current_app.cli.add_command(import_families_command)
    current_app.cli.add_command(import_proteins_command)
