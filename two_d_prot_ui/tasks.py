#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import argparse
import contextlib
import datetime
import gzip
import json
import logging
import os
import requests
import requests.adapters
import shutil
import tempfile
import time
import urllib.request
import uuid
import sys

import celery

from flask import current_app

from werkzeug.exceptions import NotFound

from . import db
from . import models
from . import loader
from . import utils
from .celery import celery_app

our_dir = os.path.dirname(os.path.realpath(__file__))
their_dir = os.path.join(our_dir, '2dprot')
sys.path.append(their_dir)

import utils_generate_svg


def _get_families():
    """
    Using latest snapshot of CATH data, return list of families.
    """
    families = set()
    url = "ftp://orengoftp.biochem.ucl.ac.uk/cath/releases/daily-release/newest/cath-b-newest-all.gz"
    file_name = os.path.basename(url)

    with tempfile.TemporaryDirectory() as workdir:
        file_path = os.path.join(workdir, file_name)

        logging.debug(f"Downloading {url} to {file_path}")
        with contextlib.closing(urllib.request.urlopen(url)) as r:
            with open(file_path, 'wb') as f:
                shutil.copyfileobj(r, f)

        logging.debug(f"Unpacking {file_path}")
        with gzip.open(file_path, 'rb') as f:

            logging.debug("Parsing file")
            for row in f.read().split(b'\n'):
                if row == b'':
                    continue
                row_split = row.split()
                try:
                    family = row_split[2]
                except IndexError:
                    logging.warning("Row %s parsing failed" % str(row))
                else:
                    families.add(family.decode())

    return families


@celery_app.task
def pdbe_api_snapshot_all_families(trail=True):
    """
    Celery task that creates snapshot directory and initiates sub-tasks for
    individual families which mirrors PDBe API content to that directory.
    It also stores initial metadata about the main snapshot task to the DB.
    """
    started_at = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
    snapshot = started_at.isoformat(timespec='seconds')

    snapshot_dir = os.path.join(current_app.config['PDBE_API_SNAPSHOTS'], snapshot)
    logging.info(f"Initiating new snapshot {snapshot} directory {snapshot_dir}")
    os.makedirs(snapshot_dir)

    families = _get_families()
    logging.info(f"For snapshot {snapshot} going to download {len(families)} families")

    logging.info("Initiating subtasks for individual families")
    group = celery.group(pdbe_api_snapshot_family.signature((snapshot_dir, f)) for f in families)
    result = group.apply_async()
    result.save()

    logging.info("Storing metadata about snapshot run into DB")
    db_task = models.PdbeApiSnapshotAllFamilies(snapshot=snapshot, status='PENDING', started_at=started_at, families_count=len(families), families_successful=0, families_failed=0, celery_id=uuid.UUID(result.id))
    db.app_db.session.add(db_task)
    db.app_db.session.commit()


@celery_app.task
def pdbe_api_snapshot_status():
    """
    Celery task that repeatedly checks status of all family snapshot sync
    tasks and their sub-tasks and updates main task's metadata in the DB.
    """
    db_tasks = models.PdbeApiSnapshotAllFamilies.query \
        .filter(models.PdbeApiSnapshotAllFamilies.status=='PENDING') \
        .order_by(models.PdbeApiSnapshotAllFamilies.started_at) \
        .all()

    for db_task in db_tasks:
        logging.debug(f"Checking status of group {db_task.celery_id} started at {db_task.started_at} with {db_task.families_count} sub tasks")
        result = celery.result.GroupResult.restore(str(db_task.celery_id))

        # Something bad happened, I can not restore my task
        if result is None:
            logging.info(f"All families sync task {db_task.celery_id} is None, marking it as lost")
            db_task.status = 'LOST'
            db.app_db.session.add(db_task)
            db.app_db.session.commit()
            continue

        # Check if this is already too old task and we can cancel it
        now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
        if now - db_task.started_at > datetime.timedelta(days=3):
            logging.info(f"All families sync task {db_task.celery_id} is too old ({now - db_task.started_at}), marking it as timeouted")
            db_task.status = 'TIMEOUTED'
            db.app_db.session.add(db_task)
            db.app_db.session.commit()
            result.revoke(terminate=True, wait=True, timeout=100)
            continue

        # Check if all the sub tasks finished and we can finish it
        families_successful = 0
        families_failed = 0
        finished_at = started_at
        for r in result:
            if r.successful():
                families_successful += 1
            elif r.failed():
                families_failed += 1
            if r.ready():
                date_done = r.date_done.replace(tzinfo=datetime.timezone.utc)
                if date_done > finished_at:
                    finished_at = date_done

        # Update task status in the DB
        if families_successful + families_failed == db_task.families_count:
            logging.info(f"All families in sync task {db_task.celery_id} finished ({families_successful} successful, {families_failed} failed), marking it as done")
            db_task.status = 'DONE'
            db_task.families_successful = families_successful
            db_task.families_failed = families_failed
            db_task.finished_at = finished_at
        else:
            logging.info(f"Not all families in sync task {task_db.celery_id} finished ({families_successful} successful, {families_failed} failed), updating counts")
            db_task.families_successful = families_successful
            db_task.families_failed = families_failed
        db.app_db.session.add(db_task)
        db.app_db.session.commit()


def _get_family_json(family, timeout=30):
    """
    Get family data from PDBe and do bunch of sanity checks on it.
    """
    url = f"https://www.ebi.ac.uk/pdbe/api/mappings/{family}"

    logging.debug(f"Trying to get {family} from {url} with timeout {timeout} seconds")
    response = requests.get(url, timeout=timeout)

    assert response.status_code == 200, f"Status code have to be 200, not {response.status_code}"
    assert isinstance(response.json(), dict), f"Content of the response needs to be JSON, not '{response.text[:100]}'"
    assert family in response.json(), f"Response JSON should contain key {family}, but it has '{str(response.json)[:100]}'"

    return response.json()


@celery_app.task()
def pdbe_api_snapshot_family(snapshot_dir, family):
    """
    Store family data JSON to snapshot directory. If there are some issues
    during getting the data, retry few times.
    """
    errors = []

    for attempt in range(3):
        try:
            response_json = _get_family_json(family)
        except Exception as e:
            logging.warning(f"Attempt {attempt + 1} to get family {family} failed with: {e}")
            errors.append(str(e))
        else:
            break
    else:
        raise Exception(f"Failed to get family {family} in {attempt + 1} attempts because of: {'; '.join(errors)}")

    with open(os.path.join(snapshot_dir, family + ".json"), 'w') as fp:
        json.dump(response_json, fp)


@celery_app.task(bind=True)
def items_custom_job(self, family, domains):
    """
    Generate multiimage for custom set of domains
    """
    attempt = 0
    attempt_max = 10
    while attempt < attempt_max:
        try:
            db_custom_job = models.CustomJob.query \
                .filter(models.CustomJob.id==uuid.UUID(self.request.id)) \
                .first_or_404()
        except NotFound:
            current_app.logger.info(f"Custom job {self.request.id} still not there in the DB on attempt {attempt}/{attempt_max}")
            attempt += 1
            time.sleep(1)
        else:
            break
    else:
        raise

    data = {}
    loader.family(data, family, 'latest')

    # Mark the job as being processed
    db_custom_job.status = 'IN PROCESS'
    db.app_db.session.add(db_custom_job)
    db.app_db.session.commit()

    # Create directory for job related data
    job_rel_dir = self.request.id
    job_dir = os.path.join(current_app.config['SOURCE_CUSTOM_JOBS'], job_rel_dir)
    job_src_dir = os.path.join(current_app.config['SOURCE_FAMILIES'], 'generated-' + data['family'].name, data['family_version'].name)
    os.makedirs(job_dir, exist_ok=True)

    # Copy required data into the directory
    shutil.copy(os.path.join(job_src_dir, 'rotation.json'), job_dir)
    for d in domains:
        d_protein, d_chain, d_domain = utils.domain_name_split(d)
        d_file = os.path.join(job_src_dir, f'layout-{d_protein}{d_chain}{d_domain}.json')
        shutil.copy(d_file, job_dir)

    # Create input data
    print("fam: multiple", family)
    args = argparse.Namespace( )
    args.add_descriptions = 'multiple'
    args.irotation = os.path.join(job_dir, 'rotation.json')
    args.itemplate = None
    args.iligands = None
    args.ligands = None
    args.layouts = [job_dir]
    args.opac = '1'
    args.orotation = None
    args.oligands = None
    args.otemplate = os.path.join(job_dir, 'ctemplate.json')
    args.oligands = None
    args.output = os.path.join(job_dir, 'imageCircle-multiple.svg')
    args.data = False

    # Compute!
    try:
        utils_generate_svg.compute(args)
    except Exception as e:
        current_app.logger.error(f"Celery task {self} failed with {type(e)}: {e}")
        db_custom_job.status = 'ERROR'
        db.app_db.session.add(db_custom_job)
        db.app_db.session.commit()
        raise
    else:
        db_custom_job.status = 'DONE'
        db.app_db.session.add(db_custom_job)
        db.app_db.session.commit()
