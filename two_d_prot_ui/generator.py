import os
import sys
our_dir = os.path.dirname(os.path.realpath(__file__))
their_dir = os.path.join(our_dir, '2dprot')
sys.path.append(their_dir)

import shutil
import uuid

from flask import (
    current_app, flash, redirect, render_template, url_for
)
from markupsafe import escape

from sqlalchemy.sql.expression import (
    and_,
)

from . import db
from . import models
from . import tasks

import utils_generate_svg


def custom_form(data):
    return render_template('items/custom-form.html', **data)


def custom_post(input_field):
    # First check if number of domains does not exceed our limit
    if len(input_field.split()) > current_app.config['CUSTOM_MAX_DOMAINS']:
            flash(f"Provided {len(input_field.split())} rows, only {current_app.config['CUSTOM_MAX_DOMAINS']} allowed", 'error')
            return redirect(url_for('items.custom'))

    # Then check if domains exists and belong to one common family
    domains = []
    db_family = None
    for domain in input_field.split():
        domain = domain.strip()
        db_domain = models.Domain.query \
            .filter(models.Domain.name==domain) \
            .first()

        if db_domain is None:
            flash(f'Domain {escape(domain)} not found in 2DProts database', 'error')
            return redirect(url_for('items.custom'))

        db_version = models.Version.query \
            .join(models.FamilyVersion) \
            .join(models.DomainFamilyVersion) \
            .filter(models.DomainFamilyVersion.domain==db_domain) \
            .order_by(models.Version.name.desc()) \
            .limit(1) \
            .first_or_404()

        if db_family is None:
            db_family = models.Family.query \
                .join(models.FamilyVersion) \
                .join(models.DomainFamilyVersion) \
                .filter(and_(models.FamilyVersion.version==db_version,
                             models.DomainFamilyVersion.domain==db_domain)) \
                .first()
        else:
            db_family_tmp = models.Family.query \
                .join(models.FamilyVersion) \
                .join(models.DomainFamilyVersion) \
                .filter(and_(models.FamilyVersion.version==db_version,
                             models.DomainFamilyVersion.domain==db_domain)) \
                .first()
            if db_family != db_family_tmp:
                flash(f'Domain {escape(d)} is from family {db_family_tmp.name} but previous domain was from {db_family.name}', 'error')
                return redirect(url_for('items.custom'))

        domains.append(db_domain)

    if len(domains) == 0:
        flash('No domains found', 'error')
        return redirect(url_for('items.custom'))

    if db_family is None:
        flash('No family found', 'error')
        return redirect(url_for('items.custom'))

    domain_names = [d.name for d in domains]

    job_id = tasks.items_custom_job.apply_async(args=[db_family.name, domain_names])

    db_custom_job = models.CustomJob(id=uuid.UUID(job_id.id), status='PENDING', family_str=db_family.name, domains_str=' '.join(domain_names))
    db.app_db.session.add(db_custom_job)
    db.app_db.session.commit()

    return redirect(url_for('items.custom', job_id=job_id))


def custom_job(job_id):
    db_custom_job = models.CustomJob.query \
        .filter(models.CustomJob.id==uuid.UUID(job_id)) \
        .first_or_404()
    data = {
        'job_id': db_custom_job.id,
        'job_status': db_custom_job.status,
        'job_created': db_custom_job.imported_at,
        'job_family': db_custom_job.family_str,
        'job_domains':  db_custom_job.domains_str.split(),
        'job_rel_dir': os.path.join(current_app.config['STATIC_CUSTOM_JOBS'], str(db_custom_job.id)),
    }
    return render_template('items/custom-job.html', **data)
