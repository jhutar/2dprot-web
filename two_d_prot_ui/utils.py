import sqlalchemy.exc

def domain_name_split(domain):
    domain_protein = domain[:4]
    domain_chain = domain[4:-2]
    domain_domain = domain[-2:]
    return (domain_protein, domain_chain, domain_domain)

def query_no_value_safe(query):
    try:
        return query()
    except sqlalchemy.exc.StatementError as e:
        if 'no value has been set for this column' in str(e):
            return None
        else:
            raise
