import os

from flask import (
    current_app,
)

from sqlalchemy.sql.expression import (
    and_,
)

from werkzeug.exceptions import (
    abort,
)

from . import models

import json


def family(data, family, version):
    print("-------------")
    print("family", family, version)
    db_family = models.Family.query \
        .filter(models.Family.name==family) \
        .first_or_404()

    if version == 'latest':
        db_version = models.Version.query \
            .join(models.FamilyVersion) \
            .filter(models.FamilyVersion.family==db_family) \
            .order_by(models.Version.name.desc()) \
            .limit(1) \
            .first_or_404()
    else:
        db_version = models.Version.query \
            .join(models.FamilyVersion) \
            .filter(and_(models.FamilyVersion.family==db_family,
                         models.Version.name==version)) \
            .first_or_404()

    family_dir = f"{current_app.config['STATIC_FAMILIES']}/generated-{db_family.name}/{db_version.name}"

    data['family'] = db_family
    data['family_version'] = db_version
    data['family_dir'] = family_dir
    split_family_set = family.split(".")
    data['h0'] = split_family_set[0]
    data['h1'] = split_family_set[1]
    data['h2'] = split_family_set[2]


def family_versions(data, family):
    db_versions = models.Version.query \
        .join(models.FamilyVersion) \
        .filter(models.FamilyVersion.family==data['family']) \
        .order_by(models.Version.name.desc()) \
        .all()
    if len(db_versions) == 0:
        abort(404, "No versions found for the family")

    data['family_versions'] = db_versions


def family_domains(data, family, version):
    db_domains = models.Domain.query \
        .join(models.DomainFamilyVersion) \
        .join(models.FamilyVersion) \
        .filter(models.FamilyVersion.family==data['family'], models.FamilyVersion.version==data['family_version']) \
        .order_by(models.Domain.name) \
        .all()
    if len(db_domains) == 0:
        abort(404, "No domains found for the family version")

    data['family_domains'] = db_domains


def family_clusters(data, family, version):
    db_clusters = models.Cluster.query \
        .join(models.ClusterFamilyVersion) \
        .join(models.FamilyVersion) \
        .filter(models.FamilyVersion.family==data['family'], models.FamilyVersion.version==data['family_version']) \
        .order_by(models.Cluster.name) \
        .all()

    data['family_clusters'] = db_clusters


def family_prev_next(data, family, version):
    db_family_next = models.Family.query \
        .order_by(models.Family.name.asc()) \
        .filter(and_(
            models.Family.name > data['family'].name,
            models.Family.versions.any(),
        )) \
        .limit(1) \
        .first()
    db_family_prev = models.Family.query \
        .order_by(models.Family.name.desc()) \
        .filter(and_(
            models.Family.name < data['family'].name,
            models.Family.versions.any(),
        )) \
        .limit(1) \
        .first()

    data['family_prev'] = db_family_prev
    data['family_next'] = db_family_next


def family_ligands(data, family, version):
    filename = f"{current_app.config['SOURCE_FAMILIES']}/generated-{family}/{os.path.basename(data['family_dir'])}/ligands.json"
    try:
        with open(filename, "r") as fd:
            ligands_struct = json.load(fd)
    except FileNotFoundError:
        return "Ligand file missing"

    ligands_table = """
    <style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    </style>

    <table style="width:90%">
    <colgroup>
        <col span="2" style="background-color:#CCCCCC">
        <col span="2" style="background-color:#EEEEEE">
        <col span="2" style="background-color:#CCCCCC">
        <col span="2" style="background-color:#EEEEEE">
        <col span="2" style="background-color:#CCCCCC">
    </colgroup>
    <tr><th>ligand</th><th>count</th><th>ligand</th><th>count</th>
        <th>ligand</th><th>count</th><th>ligand</th><th>count</th>
        <th>ligand</th><th>count</th></tr>
    """
    counter = 0
    for l in ligands_struct:
        if counter == 0:
            ligands_table += "<tr>\n"
        ligands_table += f"<td>{l['ecmp']}</td>\n            <td>{l['count']}</td>\n"
        counter += 1
        if counter == 5:
            ligands_table += "</tr>\n"
            counter = 0
    while counter != 0 and counter != 5:
        ligands_table += f"<td></td>\n            <td></td>\n"
        counter += 1

    if counter != 0:
        ligands_table += "</tr>\n"
    ligands_table += "</table>\n"
    data['family_ligands'] = ligands_table


def protein(data, protein, version):
    db_protein = models.Protein.query \
        .filter(models.Protein.name==protein) \
        .first_or_404()

    if version == 'latest':
        db_version = models.Version.query \
            .join(models.ProteinVersion) \
            .filter(models.ProteinVersion.protein==db_protein) \
            .order_by(models.Version.name.desc()) \
            .limit(1) \
            .first_or_404()
    else:
        db_version = models.Version.query \
            .join(models.ProteinVersion) \
            .filter(and_(models.ProteinVersion.protein==db_protein,
                         models.Version.name==version)) \
            .first_or_404()

    protein_dir = f"{current_app.config['STATIC_PROTEINS']}/generated-{db_protein.name}/{db_version.name}"
    protein_dir_source = f"{current_app.config['SOURCE_PROTEINS']}/generated-{db_protein.name}/{db_version.name}"

    protein_dir_source_file = os.path.join(protein_dir_source, f"{db_protein.name}.svg")
    current_app.logger.info(f"Checking path of {protein_dir_source_file}: {os.path.isfile(protein_dir_source_file)}")   # FIXME
    if os.path.isfile(protein_dir_source_file):
        protein_filename = f"{db_protein.name}.svg"
    else:   # fallback to old-style protein name
        protein_filename = f"image-{db_protein.name}.svg"
        current_app.logger.warning(f"Falling back to {protein_filename}")

    protein_dir_source_file_ligands = os.path.join(protein_dir_source, f"{db_protein.name}_l.svg")
    current_app.logger.info(f"Checking path of {protein_dir_source_file_ligands}: {os.path.isfile(protein_dir_source_file_ligands)}")   # FIXME
    if os.path.isfile(protein_dir_source_file_ligands):
        protein_ligands_filename = f"{db_protein.name}_l.svg"
    else:
        protein_ligands_filename = None
        current_app.logger.warning(f"No protein with ligands image for {protein_filename}")

    data['protein'] = db_protein
    data['protein_version'] = db_version
    data['protein_dir'] = protein_dir
    data['protein_dir_source'] = protein_dir_source
    data['protein_filename'] = protein_filename
    data['protein_ligands_filename'] = protein_ligands_filename


def protein_versions(data, protein, version):
    db_versions = models.Version.query \
        .join(models.ProteinVersion) \
        .filter(models.ProteinVersion.protein==data['protein']) \
        .order_by(models.Version.name.desc()) \
        .all()
    if len(db_versions) == 0:
        abort(404, "No versions found for the protein")

    data['protein_versions'] = db_versions


def protein_domains(data, domains_page):
    db_domains_pager = models.Domain.query \
        .filter(and_(models.Domain.name.like(f"{data['protein'].name}%"),
                     models.Domain.deleted == False)) \
        .order_by(models.Domain.name.asc()) \
        .paginate(page=domains_page, error_out=True, max_per_page=10)
    # NOTE: Some proteins do not have any domains (just ligands) so no
    # reason to return 404 if len(db_domains) == 0
    domains_list = []
    for d in db_domains_pager.items:
        domain_data = {}
        domain(domain_data, d.name, "latest")
        domains_list.append(domain_data)

    data['protein_domains_pager'] = db_domains_pager
    data['protein_domains'] = domains_list


def protein_prev_next(data, protein, version):
    db_protein_next = models.Protein.query \
        .order_by(models.Protein.name.asc()) \
        .filter(models.Protein.name > data['protein'].name) \
        .limit(1) \
        .first()
    db_protein_prev = models.Protein.query \
        .order_by(models.Protein.name.desc()) \
        .filter(models.Protein.name < data['protein'].name) \
        .limit(1) \
        .first()

    data['protein_prev'] = db_protein_prev
    data['protein_next'] = db_protein_next


def cluster(data, cluster, version):
    db_cluster = models.Cluster.query \
        .filter(models.Cluster.name==cluster) \
        .first_or_404()

    if version == 'latest':
        db_version = models.Version.query \
            .join(models.FamilyVersion) \
            .join(models.ClusterFamilyVersion) \
            .filter(models.ClusterFamilyVersion.cluster==db_cluster) \
            .order_by(models.Version.name.desc()) \
            .limit(1) \
            .first_or_404()
    else:
        db_version = models.Version.query \
            .join(models.FamilyVersion) \
            .join(models.ClusterFamilyVersion) \
            .filter(and_(models.ClusterFamilyVersion.cluster==db_cluster,
                         models.Version.name==version)) \
            .first_or_404()

    db_family = models.Family.query \
        .join(models.FamilyVersion) \
        .join(models.ClusterFamilyVersion) \
        .filter(and_(models.FamilyVersion.version==db_version,
                     models.ClusterFamilyVersion.cluster==db_cluster)) \
        .first_or_404()

    cluster_dir = f"{current_app.config['STATIC_FAMILIES']}/generated-{db_family.name}/{db_version.name}/{db_cluster.name}"
    family_dir = f"{current_app.config['STATIC_FAMILIES']}/generated-{db_family.name}/{db_version.name}"

    data['cluster'] = db_cluster
    data['cluster_version'] = db_version
    data['cluster_family'] = db_family
    data['cluster_dir'] = cluster_dir
    data['cluster_family_dir'] = family_dir

    db_clusters = models.Cluster.query \
        .join(models.ClusterFamilyVersion) \
        .join(models.FamilyVersion) \
        .filter(models.FamilyVersion.family==data['cluster_family'], models.FamilyVersion.version==data['cluster_version']) \
        .order_by(models.Cluster.name) \
        .all()
    data['cluster_family_clusters'] = db_clusters

    db_domains = models.Domain.query \
        .join(models.ClusterDomainVersion) \
        .join(models.ClusterVersion) \
        .filter(models.ClusterVersion.version==data['cluster_version'], models.ClusterVersion.cluster==data['cluster'])\
        .order_by(models.Domain.name) \
        .all()

#    if len(db_domains) == 0:
#        abort(404, "No domains found for the family version")

    data['cluster_domains'] = db_domains


def cluster_domains(data, cluster, version):
    pass


def cluster_ligands(data, cluster, version):

    filename= f"{current_app.config['SOURCE_FAMILIES']}/generated-{data['cluster_family'].name}/{os.path.basename(data['cluster_family_dir'])}/{cluster}/ligands.json"
    print("filename", filename)
    try:
        with  open(filename, "r") as fd:
            ligands_struct = json.load(fd)
    except:
        return "Ligand file missing"

    ligands_table = """
    <style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    </style>

    <table style="width:90%">
    <colgroup>
        <col span="2" style="background-color:#CCCCCC">
        <col span="2" style="background-color:#EEEEEE">
        <col span="2" style="background-color:#CCCCCC">
        <col span="2" style="background-color:#EEEEEE">
        <col span="2" style="background-color:#CCCCCC">
    </colgroup>
    <tr><th>ligand</th><th>count  </th><th>ligand</th><th>count  </th>
        <th>ligand</th><th>count  </th><th>ligand</th><th>count  </th>
        <th>ligand</th><th>count  </th></tr>
"""
    counter = 0
    for l in ligands_struct:
        if counter == 0:
            ligands_table += "<tr>\n"
        ligands_table += f"<td>{l['ecmp']}</td>\n            <td>{l['count']}</td>\n"
        counter += 1
        if counter == 5:
            ligands_table += "</tr>\n"
            counter = 0
    while counter != 0 and counter != 5:
        ligands_table += f"<td></td>\n            <td></td>\n"
        counter += 1

    if counter != 0:
        ligands_table += "</tr>\n"
    ligands_table += "</table>\n"
    data['cluster_ligands'] = ligands_table


def domain(data, domain, version):
    db_domain = models.Domain.query \
        .filter(models.Domain.name==domain) \
        .first_or_404()

    if version == 'latest':
        db_version = models.Version.query \
            .join(models.FamilyVersion) \
            .join(models.DomainFamilyVersion) \
            .filter(models.DomainFamilyVersion.domain==db_domain) \
            .order_by(models.Version.name.desc()) \
            .limit(1) \
            .first_or_404()
    else:
        db_version = models.Version.query \
            .join(models.FamilyVersion) \
            .join(models.DomainFamilyVersion) \
            .filter(and_(models.DomainFamilyVersion.domain==db_domain,
                         models.Version.name==version)) \
            .first_or_404()

    db_family = models.Family.query \
        .join(models.FamilyVersion) \
        .join(models.DomainFamilyVersion) \
        .filter(and_(models.FamilyVersion.version==db_version,
                     models.DomainFamilyVersion.domain==db_domain)) \
        .first_or_404()

    family_dir = f"{current_app.config['STATIC_FAMILIES']}/generated-{db_family.name}/{db_version.name}"

    data['domain'] = db_domain
    data['domain_version'] = db_version
    data['domain_family'] = db_family
    data['domain_family_dir'] = family_dir


def domain_versions(data, domain):
    db_versions = models.Version.query \
        .join(models.FamilyVersion) \
        .join(models.DomainFamilyVersion) \
        .filter(models.DomainFamilyVersion.domain==data['domain']) \
        .order_by(models.Version.name.desc()) \
        .all()
    if len(db_versions) == 0:
        abort(404, "No versions found for the domain")

    data['domain_versions'] = db_versions


def domain_prev_next(data, domain, version):
    db_domain_next = models.Domain.query \
        .join(models.DomainFamilyVersion) \
        .join(models.FamilyVersion) \
        .order_by(models.Domain.name.asc()) \
        .filter(and_(models.FamilyVersion.family==data['domain_family'],
                     models.FamilyVersion.version==data['domain_version'],
                     models.Domain.name > data['domain'].name)) \
        .order_by(models.Domain.name.asc()) \
        .limit(1) \
        .first()
    db_domain_prev = models.Domain.query \
        .join(models.DomainFamilyVersion) \
        .join(models.FamilyVersion) \
        .order_by(models.Domain.name.desc()) \
        .filter(and_(models.FamilyVersion.family==data['domain_family'],
                     models.FamilyVersion.version==data['domain_version'],
                     models.Domain.name < data['domain'].name)) \
        .order_by(models.Domain.name.desc()) \
        .limit(1) \
        .first()

    data['domain_prev'] = db_domain_prev
    data['domain_next'] = db_domain_next
