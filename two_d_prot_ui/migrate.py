from flask import current_app

import flask_migrate

from . import db


migrate_app = flask_migrate.Migrate(current_app, db.app_db)
