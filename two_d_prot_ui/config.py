import logging
import os
import secrets

import json


def get_config():
    config_file = os.environ.get('TWO_D_PROT_CONFIG', 'config.json')

    with open(config_file, 'r') as fp:
        config_content = json.load(fp)

    config_content['SECRET_KEY'] = secrets.token_hex()

    return config_content


def set_logging(app):
    app.logger.setLevel(logging.getLevelName(app.config.get('LOGGING', 'WARNING')))
