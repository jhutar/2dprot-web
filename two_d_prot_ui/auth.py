from flask import current_app
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash

from . import create_app

auth_app = HTTPBasicAuth()

users = {
    "prometheus": generate_password_hash(current_app.config['FLASK_PROMETHEUS_METRICS_PASSWORD']),
}

@auth_app.verify_password
def verify_password(username, password):
    if username in users and \
            check_password_hash(users.get(username), password):
        return username
