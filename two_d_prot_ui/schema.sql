DROP TABLE IF EXISTS custom_job;
DROP TABLE IF EXISTS version_fcluster_domains;
DROP TABLE IF EXISTS version;
DROP TABLE IF EXISTS af_version;
DROP TABLE IF EXISTS fcluster;
DROP TABLE IF EXISTS domain;
DROP TABLE IF EXISTS family;

CREATE TABLE family (
  name TEXT NOT NULL,
  PRIMARY KEY (name)
);

CREATE TABLE domain ( 
  name TEXT NOT NULL,
  PRIMARY KEY (name)
);

CREATE TABLE fcluster (
  name TEXT,
  family TEXT,
  version TEXT,
  FOREIGN KEY (family) REFERENCES family (name),
  FOREIGN KEY (version) REFERENCES version (name)
);

CREATE TABLE version (
  name TEXT,
  family TEXT,
  domain TEXT,
  FOREIGN KEY (family) REFERENCES family (name),
  FOREIGN KEY (domain) REFERENCES domain (name)
);

CREATE TABLE af_version (
  name TEXT,
  family TEXT,
  domain TEXT,
  FOREIGN KEY (family) REFERENCES family (name),
  FOREIGN KEY (domain) REFERENCES domain (name)
);

CREATE TABLE IF NOT EXISTS version_fcluster_domains (
  version TEXT,
  fcluster TEXT,
  domain TEXT,
  FOREIGN KEY (version) REFERENCES version (name),
  FOREIGN KEY (fcluster) REFERENCES fcluster (name),
  FOREIGN KEY (domain) REFERENCES domain (name)
);

CREATE TABLE custom_job (
  id TEXT,
  status TEXT,
  family TEXT,
  domains TEXT,
  PRIMARY KEY (id)
);
