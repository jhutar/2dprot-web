import celery
import celery.schedules

from flask import current_app

from . import create_app


celery_app = celery.Celery('two_d_prot_ui')

# Either get current app (we need to access it to throw RuntimeError if
# it does not exist) - this is the case of `flask run` where celery.py
# gets imported or create the application ourselves - this is the case
# of starting celery worker where we are starting celery.py directly
try:
    flask_app = current_app
    _ = flask_app.config
except RuntimeError:
    flask_app = create_app()

celery_config = {
    'include': ['two_d_prot_ui.tasks'],
    'broker_url': flask_app.config['CELERY_BROKER'],
    'result_backend': flask_app.config['CELERY_BACKEND'],
    'task_serializer': 'json',
    'result_serializer': 'json',
    'result_expires': 3600,
    'enable_utc': True,
    'timezone': 'UTC',
    'task_annotations': {
        'two_d_prot_ui.tasks.pdbe_api_snapshot_family': {
            'rate_limit': '10/s',
        },
    },
    'beat_schedule': {
        'snapshot-all-every-week': {
            'task': 'two_d_prot_ui.tasks.pdbe_api_snapshot_all_families',
            'schedule': celery.schedules.crontab(minute='10', hour='1', day_of_week='sat'),
        },
        'snapshot-status-every-5minutes': {
            'task': 'two_d_prot_ui.tasks.pdbe_api_snapshot_status',
            'schedule': celery.schedules.crontab(minute='*/5'),
        },
    },
}

celery_app.conf.update(**celery_config)

# Add flask app context to celery.Task
TaskBase = celery_app.Task
class ContextTask(TaskBase):
    abstract = True
    def __call__(self, *args, **kwargs):
        with flask_app.app_context():
            flask_app.logger.debug(f"Celery task {self} started with {args} and {kwargs}")
            return TaskBase.__call__(self, *args, **kwargs)
celery_app.Task = ContextTask

if __name__ == '__main__':
    celery_app.start()
