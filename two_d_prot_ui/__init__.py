import os

from flask import Flask

from werkzeug.middleware.proxy_fix import ProxyFix

from . import config


def create_app(test_config=None):
    # Create the app and make it to accept only 1 proxy in the chain
    app = Flask(__name__)
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1)

    # Configure application based on FLASK_ENV environment variable
    app.config.from_mapping(config.get_config())

    # Set logging level
    config.set_logging(app)

    # Initialize DB
    from .db import app_db
    app_db.init_app(app)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    with app.app_context():
        from .migrate import migrate_app

    with app.app_context():
        from . import cli
        cli.init_app()

    with app.app_context():
        from .auth import auth_app

    with app.app_context():
        from . import items
        app.register_blueprint(items.bp)
        app.add_url_rule('/', endpoint='index')

    return app


