{% extends 'base.html' %}

{% block header %}
  <h1>{% block title %}User Manual{% endblock %}</h1>
{% endblock %}

{% block content %}
<h2>Table of contents</h2>

<ul>
    <li><a href="#manual_diagrams">Types of 2D SSE diagrams in the 2DProts database</a><ul>
        <li><a href="#manual_diagrams_domain">2D SSE diagram of individual protein domains</a></li>
        <li><a href="#manual_diagrams_family">2D multiple SSE diagram of protein families</a></li>
        <li><a href="#manual_diagrams_protein">2D SSE diagram of individual proteins</a></li>
    </ul></li>
    <li><a href="#manual_search">Searching the 2DProts database</a></li>
    <li><a href="#2Dprot_output">2DProts output pages</a><ul>
        <li><a href="#per_domain_output">2D diagram of individual protein domains page</a></li>
        <li><a href="#per_family_output">Multiple 2D diagram of whole families page </a></li>
        <li><a href="#per_cluster_output">Multiple 2D diagram of whole clusters page</a></li>
    </ul></li>
    <li><a href="#custom_entry">Custom entry 2DProts multiple picture</a></li>
    <li><a href="#rcsb_filtering">Filtering PDB entries</a></li>
</ul>

<hr>
<h2 id="manual_diagrams">Types of 2D SSE diagrams in the 2DProts database</h2>

<p>The 2DProts database contains three types of diagrams:
the 2D SSE domain diagrams, the 2D multiple SSE family diagrams, and the 2D SSE protein diagrams.
</p>

<hr>
<h3 id="manual_diagrams_domain">2D SSE diagram of individual protein domains</h3>

<div class="row">
  <div class="col">
    <p>A 2D SSEs diagram of a domain visualizes the relative distance between SSEs,
    as well as the position of amino acids that comprise each element in the amino acid chain of the domain.</p>
    <p>The 2D diagram contains two types of symbols that represent secondary structures:</p>
    <ul>
        <li><strong>an arrow,</strong> which represents a strand</li>
        <li><strong>a bar,</strong> which represents a helix</li>
    </ul>
    <p>Both symbols can be colored in two ways:</p>
    <ul>
        <li><strong> Using rainbow colors</strong>, which are used for SSEs occurring in more than 80% of domains in its family. The exact color of an SSE
                     depends on its position in the domain. The color spectrum starts on blue (the start of the sequence that represents the domain)
                     and goes through green, yellow and orange to red (the end of the sequence that represents the domain).
        <li><strong> Using grey,</strong> which is used if the SSE is a part of less than or equal to 80% of proteins in the protein family.
    </ul>
  </div>
  <p>On the right, you can see an example of a 2D diagram of domain <a href="{{ url_for('items.domain', domain='1aomB02') }}">1aomB02</a>.</p>
  <div class="col">
      <a href="{{ url_for('items.domain', domain='1aomB02') }}"><img src="{{ url_for('static', filename='images/manual-plain.jpg') }}" width="300"/></a>
  </div>
</div>

<hr>
<h3 id="manual_diagrams_family">2D multiple SSE diagram of protein families</h3>

<p>
2D multiple SSE diagrams show 2D SSE diagrams of all domains of a
protein family in one picture. Diagrams of individual protein domains
are rotated and moved to similar positions.</p>

<p>There are four types of 2D multiple SSE diagrams in the 2DProts
database.
</p>

<h4>"Transparent" (Default)</h4>

<div class="row">
  <div class="col">
    <p>In the default type of 2D multiple SSE diagram, each domain 2D
    SSE diagram is shown with some level of transparency. Therefore,
    the unstable parts (the parts which occur in a small portion
    of domains from the given family) are less visible. The major
    impact is for families with a lot of domains.</p>
    <p>The average domain is shown as well. It contains SSEs which are
    in at least 80% of domains in the given family. The position and
    orientation of each SSE are computed as an average through all
    domains that contain this SSE. The average domain is visualized
    using filled dark grey SSEs – arrows and bars.</p>
    <p>The example on the right shows the first type of the multiple 2D diagram for protein family
    <a href = "https://2dprots.ncbr.muni.cz/family/2.140.10.20">2.140.10.20</a>.</p>
  </div>
  <div class="col">
    <a href = "https://2dprots.ncbr.muni.cz/family/2.140.10.20">
    <img src="{{ url_for('static', filename='images/manual-first.jpg') }}" width="300"/>
    </a>
  </div>
</div>

<h4>"Opaque"</h4>
<div class="row">
  <div class="col">
    <p>
    In the third type of diagram, all domain diagrams are shown with 100% visibility. Therefore, the 2D multiple SSE diagram of this type
    shows also structures that are present only in a small portion of family domains.</p>

    <p>
    The average protein is shown as well. It contains SSEs which are in at least 80% of
    protein domains in the family. The position and orientation of each SSE are computed
    as an average through all domains that contain this SSE. The average protein is 
    visualized using filled dark grey SSE symbols (arrows and bars).</p>

    <p>The example on the right shows the fourth type of the multiple 2D diagram for protein family
    <a href = "https://2dprots.ncbr.muni.cz/family/2.140.10.20">2.140.10.20</a>.</p>
  </div>

  <div class="col">
    <a href = "https://2dprots.ncbr.muni.cz/family/2.140.10.20">
    <img src="{{ url_for('static', filename='images/manual-third.jpg') }}" width="300"/>
    </a>
  </div>
</div>


<h4>"Opaque, no averages"</h4>
<div class="row">
  <div class="col">
    <p>In the third type, all domain diagrams are shown with 100% visibility.
    Therefore, 2D multiple SSE diagrams of this type show structures that are
    present only in a small number of domains visible in a standard way. 
    The average protein structure is not shown
    </p>
    <p>The example on the right shows the third type of the multiple 2D diagram for protein family
    <a href = "https://2dprots.ncbr.muni.cz/family/2.140.10.20">2.140.10.20</a>.</p>
  </div>
  <div class="col">
    <a href = "https://2dprots.ncbr.muni.cz/family/2.140.10.20">
    <img src="{{ url_for('static', filename='images/manual-second.jpg') }}" width="300"/>
    </a>
  </div>
</div>


<hr>
<h3 id="manual_diagrams_protein">2D SSE diagram of individual proteins</h3>

<div class="row">
  <div class="col">
    <p>A 2D SSEs diagram of a protein visualizes the relative distance between SSEs,
    as well as the position of amino acids that comprise each element in the amino acid chain
    of the domain.</p>

    <p>The 2D diagram contains two types of symbols that represent secondary structures:</p>

    <ul>
        <li><strong>an arrow,</strong> which represents a strand</li>
        <li><strong>a bar,</strong> which represents a helix</li>
    </ul>

    <p>Both symbols can have color corresponding to the chain to which the SSE belong.</p>
</p>
<p>The example on the right shows the third type of the 2D diagram for protein
<a href = "https://2dprots.ncbr.muni.cz/protein/1orw">1orw</a>.</p>

<hr> <hr>
<h2 id="manual_search">Searching the 2DProts database</h2>

<p>Each protein domain is represented in the 2DProts database via its identifier in the CATH format (e.g., 1r9nA01), and its 2D diagram is searchable using this identifier. It is also possible to search for all domains of a specific protein using its PDB ID (e.g., 1r9n). The search field of the 2DProts database also accepts substrings that are at least 3 characters long (e.g., 1r9n, 1r9, r9n).</p>

<p>Protein families are also represented in the 2DProts database via a CATH identifier (e.g., 2.140.10.20) that can be used in the search field to obtain a multiple 2D diagram of a family. Substrings that are at least 3 characters long are also supported (e.g., 2.140.10, 2.140, 140.10.20).</p>

<hr> <hr>
<h2 id="2Dprot_output">2DProts output pages</h2>

<div class="row">
  <div class="col">
    <h3 id="per_domain_output">2D diagram of individual protein domains page</h3>

    <p>2DProts output page for each domain contains four parts</p>

    <ul>
      <li>diagram for the given domain (on the left main part)</li>
      <li>name and 2DProt diagram of the family to which the domain belongs (on the right part, on the top)</li>
      <li>name and 2DProt diagram of the protein to which the domain belongs (on the right part, in the middle)</li>
      <li>links to old versions of diagrams of this domain (on the right part, on the bottom)</li>
    </ul>
  </div>

  <div class="col">
    <img src="{{ url_for('static', filename='images/manual-2-140-10-20_domain.jpg') }}" width="600">
  </div>
</div>

<hr>

<div class="row">
  <div class="col">
    <h3 id="per_family_output">Multiple 2D diagram of whole families page</h3>

    <p>2DProts output page for each family contains five parts</p>

    <ul>
      <li>multiple diagrams for the given family (on the left main part)</li>
      <li>CATH 3D diagram of the given family (on the right part on the top)</li>
      <li>name of all domains which belong to the given family (on the right part in the middle)</li>
      <li>name of all clusters which belong to the given family (on the right part in the middle)</li>
      <li>links to old versions of diagrams of this domain (on the right part at the bottom)</li>
    </ul>
  </div>

  <div class="col">
    <img src="{{ url_for('static', filename='images/manual-2-140-10-20_family.jpg') }}" width="600">
  </div>
</div>

<hr>

<div class="row">
  <div class="col">
    <h3 id="per_cluster_output">Multiple 2D diagram of whole clusters page</h3>

    <p>2DProts output page for each family cluster contains three parts</p>

    <ul>
      <li>multiple diagrams for the given family cluster (on the left main part)</li>
      <li>name and 2DProt diagram of the family to which the domain belongs (on the right part in the middle)</li>
      <li>links to old versions of diagrams of this domain (on the right part in the middle)</li>
    </ul>
  </div>

  <div class="col">
    <img src="{{ url_for('static', filename='images/manual-2-140-10-20_cluster.jpg') }}" width="600">
  </div>
</div>

<h3 id="custom_entry">Custom entry 2DProts multiple pictures</h3>

<p>In the section "Custom entry" you can create a custom multi-image from provided domains. All the domains in their latest version have to be from one family.</p>

<h3 id="rcsb_filtering">Filtering PDB entries</h3>

<p>See <a href="{{ url_for('static', filename='RCSB-filtering.pdf') }}">RCSB filtering guide (PDF, 600 kB)</a> for information on filtering PDB entries by experimental method and resolution using RCSB advanced search.</p>

{% endblock %}
