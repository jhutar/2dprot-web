import json
import os
import os.path
import re

from flask import (
    Blueprint,
    Response,
    current_app,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
)

from sqlalchemy.sql.expression import (
    and_,
)

from werkzeug.exceptions import abort, NotFound

from . import generator
from . import loader
from . import models


bp = Blueprint('items', __name__)


@bp.route('/')
def index():
    current_app.logger.debug("Hello")
    families = models.Family.query.filter_by(deleted=False).count()
    proteins = models.Protein.query.filter_by(deleted=False).count()
    domains = models.Domain.query.filter_by(deleted=False).count()
    data = {
        'families': families,
        'proteins': proteins,
        'domains': domains,
    }
    return render_template('items/index.html', **data)


@bp.route('/manual')
def manual():
    return render_template('items/manual.html')


@bp.route('/apidoc')
def apidoc():
    return render_template('items/apidoc.html')


@bp.route('/family/<string:family>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/family/<string:family>/<string:version>', methods=('GET',))
def family(family, version):
    data = {}
    loader.family(data, family, version)
    loader.family_versions(data, family)
    loader.family_domains(data, family, version)
    loader.family_clusters(data, family, version)
    loader.family_prev_next(data, family, version)
    return render_template('items/family.html', **data)


@bp.route('/family/<string:family>_l', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/family/<string:family>_l/<string:version>', methods=('GET',))
def family_l(family, version):
    data = {}
    loader.family(data, family, version)
    loader.family_versions(data, family)
    loader.family_domains(data, family, version)
    loader.family_clusters(data, family, version)
    loader.family_prev_next(data, family, version)
    loader.family_ligands(data, family, version)
    return render_template('items/family_l.html', **data)


@bp.route('/family/<string:family>_pdb2', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/family/<string:family>_pdb2/<string:version>', methods=('GET',))
def family_pdb2(family, version):
    data = {}
    loader.family(data, family, version)
    loader.family_versions(data, family)
    loader.family_domains(data, family, version)
    loader.family_clusters(data, family, version)
    loader.family_prev_next(data, family, version)
    loader.family_ligands(data, family, version)
    return render_template('items/family_pdb2.html', **data)


@bp.route('/family/<string:family>_pdb3', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/family/<string:family>_pdb3/<string:version>', methods=('GET',))
def family_pdb3(family, version):
    data = {}
    loader.family(data, family, version)
    loader.family_versions(data, family)
    loader.family_domains(data, family, version)
    loader.family_clusters(data, family, version)
    loader.family_prev_next(data, family, version)
    loader.family_ligands(data, family, version)
    return render_template('items/family_pdb3.html', **data)


@bp.route('/family/<string:family>_af4', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/family/<string:family>_af4/<string:version>', methods=('GET',))
def family_af4(family, version):
    data = {}
    loader.family(data, family, version)
    loader.family_versions(data, family)
    loader.family_domains(data, family, version)
    loader.family_clusters(data, family, version)
    loader.family_prev_next(data, family, version)
    loader.family_ligands(data, family, version)
    return render_template('items/family_af4.html', **data)


@bp.route('/family/<string:family>_af5', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/family/<string:family>_af5/<string:version>', methods=('GET',))
def family_af5(family, version):
    data = {}
    loader.family(data, family, version)
    loader.family_versions(data, family)
    loader.family_domains(data, family, version)
    loader.family_clusters(data, family, version)
    loader.family_prev_next(data, family, version)
    loader.family_ligands(data, family, version)
    return render_template('items/family_af5.html', **data)


@bp.route('/files/family/<string:family>/<string:file>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/files/family/<string:family>/<string:version>/<string:file>', methods=('GET',))
def family_files(family, version, file):
    # Ensure file is valid
    if file == 'multiple.svg':
        file = "imageCircle-multiple.svg"
    elif file == 'multiple2.svg':
        file = "imageCircle-multiple2.svg"
    elif file == 'multiple3.svg':
        file = "imageCircle-multiple3.svg"
    elif file == 'multiple4.svg':
        file = "imageCircle-multiple4.svg"
    elif file == 'multiple5.svg':
        file = "imageCircle-multiple5.svg"
    elif file == 'multiple.png':
        file = "imageCircle-multiple.png"
    elif file == 'multiple2.png':
        file = "imageCircle-multiple2.png"
    elif file == "domain_list.txt":
        file = "domain_list.txt"
    elif file == "rotation.json":
        file = "rotation.json"
    elif file == "ligands.json":
        file = "ligands.json"
    else:
        abort(404, "Invalid file {0} choice.".format(file))

    data = {}
    loader.family(data, family, version)
    return redirect(url_for("static", filename=f"{data['family_dir']}/{file}"), code=302)


@bp.route('/family/<string:family>/<string:version>.csv', methods=('GET',))
def family_csv(family, version):
    data = {}
    loader.family(data, family, version)
    loader.family_domains(data, family, version)

    domains_csv = '\n'.join([i.name for i in data['family_domains']])
    return Response(domains_csv, mimetype='text/csv')


@bp.route('/protein/<string:protein>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/protein/<string:protein>/<string:version>', methods=('GET',))
def protein(protein, version):
    data = {}
    loader.protein(data, protein, version)
    loader.protein_versions(data, protein, version)
    loader.protein_domains(data, domains_page=int(request.args.get("domains_page", 1)))
    loader.protein_prev_next(data, protein, version)
    return render_template('items/protein.html', **data)


@bp.route('/protein/<string:protein>_l', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/protein/<string:protein>_l/<string:version>', methods=('GET',))
def protein_l(protein, version):
    data = {}
    loader.protein(data, protein, version)
    loader.protein_versions(data, protein, version)
    loader.protein_domains(data, domains_page=int(request.args.get("domains_page", 1)))
    loader.protein_prev_next(data, protein, version)
    return render_template('items/protein_l.html', **data)


@bp.route('/files/protein/<string:protein>/<string:file>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/files/protein/<string:protein>/<string:version>/<string:file>', methods=('GET',))
def protein_files(protein, version, file):
    # Ensure file is valid
    if file == "svg":
        pass
    elif file == "svg_l":
        pass
    elif file == "chains.json":
        file = "chains.json"
    else:
        abort(404, "Invalid file {0} choice.".format(file))

    data = {}
    loader.protein(data, protein, version)

    if file == "chains.json":
        out = {
            "protein": data['protein'].name,
            "version": data['protein_version'].name,
            "chains": [],
        }
        pattern = re.compile(current_app.config['SOURCE_PROTEINS_CHAIN_SVG_REGEXP'])
        pattern_old = re.compile(current_app.config['SOURCE_PROTEINS_CHAIN_SVG_REGEXP_OLD'])
        protein_dir = f"{current_app.config['SOURCE_PROTEINS']}/generated-{data['protein'].name}/{data['protein_version'].name}"
        for f in os.listdir(protein_dir):
            if os.path.isfile(os.path.join(protein_dir, f)):
                match = pattern.search(f)
                if match:
                    out['chains'].append({"chain": match[2]})
                match = pattern_old.search(f)
                if match:
                    out['chains'].append({"chain": match[2]})
        return jsonify(out)
    else:
        if file == "svg":
            f = os.path.join(data['protein_dir'], data['protein_filename'])
        elif file == "svg_l":
            f = os.path.join(data['protein_dir'], data['protein_ligands_filename'])
        else:
            abort(404, "Invalid file {0} choice.".format(file))
        return redirect(url_for("static", filename=f, code=302))


@bp.route('/cluster/<string:cluster>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/cluster/<string:cluster>/<string:version>', methods=('GET',))
def cluster(cluster, version):
    data = {}
    loader.cluster(data, cluster, version)
    loader.cluster_domains(data, cluster, version)
    return render_template('items/cluster.html', **data)


@bp.route('/cluster/<string:cluster>_l', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/cluster/<string:cluster>_l/<string:version>', methods=('GET',))
def cluster_l(cluster, version):
    data = {}
    loader.cluster(data, cluster, version)
    loader.cluster_domains(data, cluster, version)
    loader.cluster_ligands(data, cluster, version)
    return render_template('items/cluster_l.html', **data)


@bp.route('/files/cluster/<string:cluster>/<string:file>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/files/cluster/<string:cluster>/<string:version>/<string:file>', methods=('GET',))
def cluster_files(cluster, version, file):
    # Ensure file is valid
    if file == "ligands.json":
        file = "ligands.json"
    else:
        abort(404, "Invalid file {0} choice.".format(file))

    data = {}
    loader.cluster(data, cluster, version)
    return redirect(url_for("static", filename=f"{data['cluster_dir']}/{file}"), code=302)


@bp.route('/domain/<string:domain>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/domain/<string:domain>/<string:version>', methods=('GET',))
def domain(domain, version):
    data = {}
    loader.domain(data, domain, version)
    loader.domain_versions(data, data['domain'].name)
    loader.domain_prev_next(data, domain, version)
    try:
        loader.protein(data, data['domain'].get_protein_name(), "latest")
    except NotFound:
        current_app.logger.warning(f"Protein {data['domain'].get_protein_name()} not found for domain {data['domain']}")
    return render_template('items/domain.html', **data)


@bp.route('/domain/<string:domain>_l', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/domain/<string:domain>_l/<string:version>', methods=('GET',))
def domain_l(domain, version):
    data = {}
    loader.domain(data, domain, version)
    loader.domain_versions(data, data['domain'].name)
    loader.domain_prev_next(data, domain, version)
    try:
        loader.protein(data, data['domain'].get_protein_name(), "latest")
    except NotFound:
        current_app.logger.warning(f"Protein {data['domain'].get_protein_name()} not found for domain {data['domain']}")
    return render_template('items/domain_l.html', **data)


@bp.route('/files/domain/<string:domain>/<string:file>', methods=('GET',), defaults={'version': 'latest'})
@bp.route('/files/domain/<string:domain>/<string:version>/<string:file>', methods=('GET',))
def domain_files(domain, version, file):
    # Ensure file is valid
    if file == 'svg':
        file = f"{domain[0:4]}_{domain[4:]}.svg"
    elif file == 'json':
        file = f"{domain[0:4]}_{domain[4:]}.json"
    elif file == 'layout.json':
        file = f"{domain[0:4]}{domain[4:]}.json"
    else:
        abort(404, "Invalid file {0} choice.".format(file))

    data = {}
    loader.domain(data, domain, version)
    return redirect(url_for("static", filename=f"{data['domain_family_dir']}/{file}"), code=302)


@bp.route('/search', methods=('POST',))
def search():
    data = {}
    print("jdu hledat")
    query = request.form['query']
    print("query je", query)
    if len(query) < 3:
        flash("Please provide at least 3 letters when searching")
    else:
        domains = models.Domain.query.filter(models.Domain.name.contains(query)).all()
        proteins = models.Protein.query.filter(models.Protein.name.contains(query)).all()

        families = models.Family.query.filter(
            and_(
                models.Family.name.contains(""),
                models.Family.versions.any(),
            )
        ).all()
        print("families 11", families, models.Family.name.contains(""), models.Family.versions.any(),)

        families = models.Family.query.filter(
            and_(
                models.Family.name.contains(query),
                models.Family.versions.any(),
            )
        ).all()
        print("families", families)
        data = {
            'query': query,
            'domains': domains,
            'proteins': proteins,
            'families': families,
        }

    return render_template('items/search.html', **data)


@bp.route('/custom', methods=('GET', 'POST'), defaults={'job_id': None})
@bp.route('/custom/<string:job_id>', methods=('GET',))
def custom(job_id):
    if request.method == 'GET' and job_id is None:
        return generator.custom_form({'max_domains': current_app.config['CUSTOM_MAX_DOMAINS']})
    if request.method == 'POST':
        return generator.custom_post(request.form['input'])
    if job_id is not None:
        return generator.custom_job(job_id)


@bp.route('/list/family', methods=('GET',))
def list_family():
    db_families_pager = models.Family.query \
        .order_by(models.Family.name) \
        .paginate(error_out=True, max_per_page=100)
    data = {
        'entity': 'family',
        'pager': db_families_pager,
    }

    return render_template('items/list_entity.html', **data)


@bp.route('/list/protein', methods=('GET',))
def list_protein():
    db_proteins_pager = models.Protein.query \
        .order_by(models.Protein.name) \
        .paginate(error_out=True, max_per_page=100)
    data = {
        'entity': 'protein',
        'pager': db_proteins_pager,
    }

    return render_template('items/list_entity.html', **data)


@bp.route('/list/domain', methods=('GET',))
def list_domain():
    db_domains_pager = models.Domain.query \
        .order_by(models.Domain.name) \
        .paginate(error_out=True, max_per_page=100)
    data = {
        'entity': 'domain',
        'pager': db_domains_pager,
    }

    return render_template('items/list_entity.html', **data)


@bp.route('/list/custom', methods=('GET',))
def list_custom():
    db_custom_job_pager = models.CustomJob.query \
        .order_by(models.CustomJob.imported_at) \
        .paginate(error_out=True, max_per_page=100)
    data = {
        'entity': 'custom',
        'parameter': 'job_id',
        'pager': db_custom_job_pager,
    }

    return render_template('items/list_entity.html', **data)


@bp.route('/api/v1/pdbe_api_snapshot_family', methods=('GET',))
def list_pdbe_api_snapshot_family():
    db_tasks = models.PdbeApiSnapshotAllFamilies.query \
        .order_by(models.PdbeApiSnapshotAllFamilies.started_at) \
        .paginate(error_out=True, max_per_page=100)

    return jsonify([i.as_dict() for i in db_tasks.items])


@bp.route('/api/v1/pdbe_api_snapshot_family/<string:snapshot>/<string:family>', methods=('GET',))
def get_pdbe_api_snapshot_family(snapshot, family):
    file_path = os.path.join(current_app.config['PDBE_API_SNAPSHOTS'], snapshot, family + ".json")
    try:
        with open(file_path, 'r') as fp:
            family_snapshot = json.load(fp)
            return jsonify(family_snapshot)
    except FileNotFoundError:
        abort(404, "Was not able to find family {family} in snapshot {snapshot}")
