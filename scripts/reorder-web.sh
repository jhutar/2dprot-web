#!/bin/bash

set -e

DATA='instance/2DProt'
FAMILY_REGEXP='[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+'
VERSION_REGEXP='[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}T[0-9]\{2\}_[0-9]\{2\}_[0-9]\{2\}_[0-9]\{0,10\}_[0-9]\{2\}_[0-9]\{2\}'
WD=$( pwd )

# Remove possibly forgotten links
echo "DEBUG: Removing forgotten links" >&2
find $DATA/ -maxdepth 1 -type l -name generated-\* -delete

# Put unordered versioned dirs to family dirs
echo "INFO: Put unordered versioned dirs to family dirs" >&2
find $DATA/ -maxdepth 1 -type d -name generated-\* | while read d; do
    echo "DEBUG: Reordering $d" >&2
    family=$( echo "$d" | sed "s|^$DATA/generated-\($FAMILY_REGEXP\)-\?.*$|\1|" )
    version=$( echo "$d" | sed "s|^$DATA/generated-$FAMILY_REGEXP-\?\(.*\)$|\1|" )

    if [ -z "$( ls -A $d )" ]; then
        rmdir "$d"
        echo "INFO: Deleted $d as it was empty directory" >&2
        continue
    fi

    if ! echo "$family" | grep --quiet "^$FAMILY_REGEXP$"; then
        echo "ERROR: Failed to recognize '$d'" >&2
        continue
    fi

    if [ -z "$version" ]; then
        echo "DEBUG: Directory '$d' do not need reordering"
        continue
    fi

    family_dir="$DATA/generated-$family"
    version_dir="$family_dir/$version"
    mkdir -p "$family_dir"

    mv "$d" "$version_dir"
    echo "DEBUG: Moved '$d' to '$version_dir'" >&2
done

# Shortening too long version dirs
echo "INFO: Shortening too long version dirs" >&2
find $DATA/ -maxdepth 2 -type d -regextype grep -regex ".*/generated-$FAMILY_REGEXP/$VERSION_REGEXP-.*" | while read d; do
    dd=$( dirname "$d" )/$( basename "$d" | sed "s/^\($VERSION_REGEXP\).*/\1/" )
    sudo mv "$d" "$dd"
    echo "DEBUG: Renamed '$d' to '$dd'" >&2
done

# Removing version dir in version dir
echo "INFO: Removing version in version dirs" >&2
find $DATA/ -maxdepth 3 -type d -regextype grep -regex ".*/generated-$FAMILY_REGEXP/$VERSION_REGEXP/$VERSION_REGEXP.*" -print | while read d; do
    sudo rm -rf "$d"
    echo "WARNING: Deleted version dir in version dir '$d'" >&2
done

# Deleting empty version dirs
echo "INFO: Deleting empty version dirs" >&2
find $DATA/ -maxdepth 2 -empty -type d -regextype grep -regex ".*/generated-$FAMILY_REGEXP/$VERSION_REGEXP" -print | while read d; do
    sudo rm -rf "$d"
    echo "WARNING: Deleted empty version dir '$d'" >&2
done

# Recreate links to latest multi images
echo "INFO: Recreate links to latest multi images" >&2
find $DATA/ -maxdepth 1 -type d -name generated-\* | grep ".*/generated-$FAMILY_REGEXP$" | while read d; do
    echo "DEBUG: Setting latest dirs for $d" >&2
    cd "$d"
    for f in imageCircle-multiple2.png imageCircle-multiple2.svg imageCircle-multiple.png imageCircle-multiple.svg; do
        if [ -L "$f" ]; then
            rm -f "$f"
        fi

        if [ -e "$f" ]; then
            echo "WARNING: File '$f' is not a link, skipping it" >&2
            continue
        fi

        latest=$( find . -maxdepth 2 -name $f | sort | tail -n 1 )

        if [ -n "$latest" ]; then
            ln -s "$latest" "$f"
        else
            echo "WARNING: Failed to find latest '$f' in '$d', skipping it" >&2
        fi
    done
    cd "$WD"
done

# Ensure we have correct perms and owners
echo "INFO: Ensure we have correct perms and owners" >&2
sudo chown centos:apache -R $DATA
sudo find $DATA/ -type d -exec chmod 755 '{}' +
sudo find $DATA/ -type f -exec chmod 644 '{}' +
